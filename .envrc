#
# environment variables
#
export PROJECT_ROOT=$(pwd)
export DIRENV_PATH="$PROJECT_ROOT/.direnv"
export TOOLS_PATH="$DIRENV_PATH/bin"

#
# globals
#
export SECRETS_FILE="$PROJECT_ROOT/ansible/secrets/credentials.yaml"

#
# helper
#
export IS_MAINTAINER="$(test -e $PROJECT_ROOT/.vault_pass && echo yes)"
export VAULT_VIEW="$(poetry run -q ansible-vault view $SECRETS_FILE)"

#
# required
#
if test -z "$IS_MAINTAINER"; then
    echo -e "\e[33m💡 Create '.vault_pass' in project root '$PROJECT_ROOT' to activate Testing Farm maintainer access."
fi

#
# setup
#
if [ ! -e "$DIRENV_PATH" ]; then
  source setup/environment.sh
else
  echo -e "\e[32m💡 Use 'make clean; direnv allow' to reinstall\e[0m"
fi

#
# tools
#
PATH_add "$TOOLS_PATH"
PATH_add "$(poetry run bash -c 'echo $VIRTUAL_ENV')/bin"

#
# Configuration below is only for maintainers
#
test -z "$IS_MAINTAINER" && return

#
# aws
#
export AWS_CONFIG_FILE="$DIRENV_PATH/.aws/config"
export AWS_SHARED_CREDENTIALS_FILE="$DIRENV_PATH/.aws/credentials"

#
# kubectl
#
export KUBECONFIG="$DIRENV_PATH/.kube/config"
export KREW_ROOT="$DIRENV_PATH/.krew/"
PATH_add "$DIRENV_PATH/.krew/bin"

#
# terraform
#
export TF_TOKEN_app_terraform_io=$(echo "$VAULT_VIEW" | yq -er .credentials.terraform.cloud.testing_farm_bot.token)
export TF_VAR_terraform_api_url=$(echo "$VAULT_VIEW" | yq -r .credentials.terraform.cloud.api)
export TF_VAR_ansible_vault_password_file="$PROJECT_ROOT/.vault_pass"
export TF_VAR_ansible_vault_credentials="credentials.yaml"
export TF_VAR_ansible_vault_secrets_root="$(dirname $SECRETS_FILE)"
export TF_VAR_gitlab_testing_farm_bot=$(echo "$VAULT_VIEW" | yq -er .credentials.gitlab.testing_farm_bot_terragrunt.token)

#
# Testing Farm
#
export TESTING_FARM_API_URL=$(echo "$VAULT_VIEW" | yq -r .credentials.testing_farm.production.api_url)
export TESTING_FARM_API_TOKEN_PUBLIC=$(echo "$VAULT_VIEW" | yq -er .credentials.testing_farm.production.public.users.bot.token)
# NOTE: this is not used currently, but we plan to use it later
export TESTING_FARM_API_TOKEN_REDHAT=$(echo "$VAULT_VIEW" | yq -er .credentials.testing_farm.production.redhat.bot_api_key)
# Needed for development operations
export TESTING_FARM_DEV_TOKEN_PUBLIC_DEVELOPER=$(echo "$VAULT_VIEW" | yq -r .credentials.testing_farm.dev.public.users.developer.token)
export TESTING_FARM_DEV_TOKEN_PUBLIC_ADMIN=$(echo "$VAULT_VIEW" | yq -r .credentials.testing_farm.dev.public.users.admin.token)
export TESTING_FARM_DEV_TOKEN_PUBLIC_WORKER=$(echo "$VAULT_VIEW" | yq -r .credentials.testing_farm.dev.public.users.worker.token)
export TESTING_FARM_DEV_TOKEN_PUBLIC_DISPATCHER=$(echo "$VAULT_VIEW" | yq -r .credentials.testing_farm.dev.public.users.dispatcher.token)
export TESTING_FARM_DEV_API_URL_PUBLIC="http://api.dev-$(whoami).testing-farm.io"

#
# gitlab-ci-linter
#
export GITLAB_PRIVATE_TOKEN=$(echo "$VAULT_VIEW" | yq -er .credentials.gitlab.testing_farm_bot.token)

#
# cleanup
#
unset VAULT_VIEW
