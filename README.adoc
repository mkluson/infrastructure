= Testing Farm Infrastructure
:data-uri:
:icons: font
:showtitle:
:source-highlighter: highlightjs
:toc:

This repository contains tools, configuration ands test used to maintain and setup Testing Farm's infrastrucutre.

The repository tries to bundle all required tools and provides an easy way how to set everything up for newcomers.

== Setup

The repository is managed via `direnv`. For setup:

* Add `.vault_pass` file with the known secret
+
[TIP]
====
Check Bitwarden for the vault password.
====
+
[source,shell]
....
echo "SECRET_PASSWORD" > .vault_pass
....
+
* Install `direnv`
+
[source,shell]
....
$ dnf -y install direnv
....
* Hook `direnv` to your shell, see the https://direnv.net/docs/hook.html[official guide for more information].
* Install required system dependencies
+
[WARNING]
====
Only Fedora system is supported at this time.
====
+
[source,shell]
....
make install/fedora
....
+
* Allow `direnv` in this directory
+
[source,shell]
....
$ direnv allow
....
+
[TIP]
====
In case of errors, investigate the setup log in `.direnv/envrc.log`.
You can try to reinstall the environment by removing the `.direnv` directory and running `direnv allow`.
====
+
* Check if kubectl works and lists all available clusters:
+
[source,shell]
....
$ kubectl ctx
....

== Make

We use `make` to manage common actions.
Run the command to see the available targets with descriptions.

== Terragrunt and Terraform

We use https://www.terraform.io/[Terraform] to setup the infrastructure.
To make the Terraform code DRY we use https://terragrunt.gruntwork.io/[Terragrunt].

The infrastructure code is located in the link:terragrunt/[terragrunt/] directory.

== Development Environment

Development environment is intended for development of infrastructure and testing changes without influencing any other environment.
The state of the infrastructure is saved locally, i.e. it uses the https://developer.hashicorp.com/terraform/language/settings/backends/local[`local` terraform backend].

Currently, the environment contains:

* AWS EKS kubernetes cluster
* Artemis instance

The name of the cluster and Artemis API endpoint is generated according to your local username.

For example, for the user `mvadkert`:

* the cluster name is `testing-farm-dev-mvadkert`
* the Artemis API endpoint is `artemis.dev-mvadkert.testing-farm.io`

To provision a complete infrastructure on Fedora AWS for development:

* initialize the `dev` environment
+
[source,shell]
....
make dev/init
....
+
* create development environment with your own AWS EKS cluster and all available services in development mode
+
[source,shell]
....
make dev/apply
....
+
* develop & have fun
* tier down the infrastructure once happy
+
[source,shell]
....
make dev/destroy
....

== Staging Environment

The staging environment serves as a preview of our production environment.
The state of the infrastructure is saved to Terraform Cloud.

Currently, the environment contains:

* AWS EKS kubernetes cluster
* Artemis instance

Other components are not yet maintained here.

Details of the deployment:

* the cluster name is `testing-farm-staging`
* the Artemis API endpoint is `artemis.dev-mvadkert.testing-farm.io`

== Integration Tests

=== Worker integration tests

We currently support running worker integration tests against the <<Development Environment>> or <<Staging Environment>>.

* to run all worker tests against your local <<Development Environment>> run `make test/dev/worker`
* to run only guest-setup worker tests against your local <<Development Environment>> run `make test/dev/guest-setup`

* to run all worker tests against <<Staging Environment>> run `make test/staging/worker`
* to run only guest-setup worker tests against <<Staging Environment>> run `make test/staging/guest-setup`

== Updating composes

To update composes on Public Ranch:

* run `make compose-update-public`
* submit a MR with the changes
* the changes will be deployed once the MR is merged
